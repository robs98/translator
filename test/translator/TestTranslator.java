package translator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestTranslator {

	@Test
	//Test user story 1
	public void testGetSentence() {
		String sentence = "hello world";
		Translator t = new Translator(sentence);
		assertEquals(sentence,t.getSentence());
	}
	
	
	@Test
	//Tests user story 2
	public void testTranslateEmptySentence() {
		String sentence ="";
		Translator t = new Translator(sentence);
		assertEquals("nil",t.translate());
	}
	
	@Test
	//Tests user story 3
	public void testTranslateStartVowelEndsY() {
		String sentence ="array";
		Translator t = new Translator(sentence);
		assertEquals(sentence+"nay",t.translate());
		
	}
}
