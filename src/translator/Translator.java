package translator;

import java.util.regex.Pattern;

public class Translator {
	protected String sentence;
	
	public enum Vowel {
		A,E,I,O,U;
	}

	public Translator(String sentence) {
		this.sentence = sentence;
	}

	public String getSentence() {
		return this.sentence;
	}

	public String translate() {
		String result = "";
		String end = "nay";
		if(this.sentence.equalsIgnoreCase("")) {
			result = "nil";
		}
		
		 if(Pattern.compile("^[aeiou][a-z]*y$",Pattern.CASE_INSENSITIVE).matcher(sentence).find()) {
			result = sentence+end;
		}
		 return result;
	}
	
	
	
}
